#! /bin/bash

# Set variables
FILES=/vagrant/provisioning/files


echo "Populating Bank Sample Data Index to elasticsearch ..."

curl -XPOST 'localhost:9200/bank/account/_bulk?pretty' --data-binary @${FILES}/accounts.json
curl 'localhost:9200/_cat/indices?v'
curl 'localhost:9200/_cat/indices?v'