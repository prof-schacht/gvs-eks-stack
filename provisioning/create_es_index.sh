#! /bin/bash

echo "Populating twitter2 Index to elasticsearch ..."
curl -XPUT 'http://localhost:9200/twitter2/' -d '
{
	"settings": {
	    "number_of_shards" : 2,
        "number_of_replicas" : 1
	},

    "mappings" : {
      "tweets" : {
        "properties" : {
          "author" : {
            "type" : "string"
          },
          "coordinates" : {
            "properties" : {
              "coordinates" : {
                "type" : "double"
              },
              "type" : {
                "type" : "string"
              }
            }
          },
          "date" : {
            "type": "date",
            "format": "EE MMM d HH:mm:ss Z yyyy"
          },
          "followerscount" : {
            "type" : "long"
          },
          "geo" : {
            "properties" : {
              "coordinates" : {
                "type" : "double"
              },
              "type" : {
                "type" : "string"
              }
            }
          },
          "geoenabled" : {
            "type" : "boolean"
          },
          "message" : {
            "type" : "string"
          },
          "polarity" : {
            "type" : "double"
          },
          "retweeted" : {
            "type" : "boolean"
          },
          "sentiment" : {
            "type" : "string"
          },
          "subjectivity" : {
            "type" : "double"
          }
        }
      }
    }
}'