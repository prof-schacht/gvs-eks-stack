#! /bin/bash

FILES=/vagrant/provisioning/files

KIBANA="kibana-4.1.2-linux-x64.tar.gz"

# Install kibana
	echo "Installing Kibana from ${KIBANA}..."
	mkdir -p /srv/kibana
	tar -C /srv/kibana -zxf ${FILES}/${KIBANA} --strip 1
	
cp /vagrant/provisioning/files/kibana /etc/init.d/kibana4
	# below is to fix the problem of CRLF (DOS format) to LF format
	perl -pi -e 's/\r\n/\n/g' /etc/init.d/kibana4
	chmod +x /etc/init.d/kibana4
	update-rc.d kibana4 defaults 99 10
	
service kibana4 start