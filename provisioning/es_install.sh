#! /bin/bash

# Set variables
FILES=/vagrant/provisioning/files

# Install java 7
jre="jre-7u79-linux-x64.tar.gz"

tar -C /usr/local -zxf ${FILES}/${jre}
ln -snf /usr/local/jre1.7.0_79 /usr/local/java

sudo dpkg -i ${FILES}/elasticsearch-1.7.2.deb
echo "export JAVA_HOME=/usr/local/java" >> /etc/default/elasticsearch

sudo update-rc.d elasticsearch defaults 95 10
sudo /etc/init.d/elasticsearch start

	echo "Waiting for elasticsearch to startup ..."
	until curl -s --connect-timeout 1 localhost:9200; do
		echo "."
		sleep 1
	done
	
# Install Kopf Plugin
echo "Installing ES Plugins ...."
sudo /usr/share/elasticsearch/bin/plugin --install lmenezes/elasticsearch-kopf/1.0
sudo /usr/share/elasticsearch/bin/plugin --install royrusso/elasticsearch-HQ
echo "Installing ES Plugins finished"

# Install stream2es into /usr/local/bin
echo "Installing stream2es ..."
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/local/jre1.7.0_79/bin/java" 1
sudo update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/local/jre1.7.0_79/bin/javaws" 1

sudo update-alternatives --set "java" "/usr/local/jre1.7.0_79/bin/java"
sudo update-alternatives --set "javaws" "/usr/local/jre1.7.0_79/bin/javaws"

sudo cp ${FILES}/stream2es /usr/local/bin
sudo chmod +x /usr/local/bin/stream2es