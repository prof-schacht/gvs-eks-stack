__author__ = 'sschacht'


import tweepy
from tweepy import OAuthHandler
from textblob import TextBlob
from elasticsearch import Elasticsearch

# ******************* Config Section ***********************

# you have to create a file called config.py in the same directory with the following information:
##  consumer_key="your key"
##  consumer_secret="your key"

## # After the step above, you will be redirected to your app's page.
## # Create an access token under the the "Your access token" section
##  access_token="your key"
##  access_token_secret="your key"

# import twitter keys and tokens
from config import consumer_key
from config import consumer_secret
from config import access_token
from config import access_token_secret

# Suchbegriffe - Einzelne Begriffe mit OR trennen. Bei und suche einfach nur Leerzeichen zwischen den Woertern.
search_terms = "safeHarbor"

# ******************* End Config ****************************


# create instance of elasticsearch
es = Elasticsearch([{'host' : 'localhost', 'port': 9200}])



if __name__ == '__main__':
    # set twitter keys/tokens
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)


    for data in tweepy.Cursor(api.search,q=search_terms).items():
        print data._json

        dict_data = data._json
        print dict_data["text"]
        # pass tweet into TextBlob
        tweet = TextBlob(dict_data["text"])
        # output sentiment polarity
        print tweet.sentiment.polarity
        # determine if sentiment is positive, negative, or neutral
        if tweet.sentiment.polarity < 0:
            sentiment = "negative"
        elif tweet.sentiment.polarity == 0:
            sentiment = "neutral"
        else:
            sentiment = "positive"
        # output sentiment
        print sentiment

         # add text and sentiment info to elasticsearch
        es.index(index="twitter2",
                 doc_type="tweets",
                 body={"author": dict_data["user"]["screen_name"],
                       "date": dict_data["created_at"],
                       "message": dict_data["text"],
                       "polarity": tweet.sentiment.polarity,
                       "subjectivity": tweet.sentiment.subjectivity,
                       "sentiment": sentiment,
                       "geoenabled": dict_data["user"]["geo_enabled"],
                       "coordinates": dict_data["coordinates"],
                       "geo" : dict_data["geo"],
                       "followerscount" : dict_data["user"]["followers_count"],
                       "retweeted" : dict_data["retweeted"]})
