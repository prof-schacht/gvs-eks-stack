__author__ = 'sschacht'

import json
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from textblob import TextBlob
from elasticsearch import Elasticsearch
import time
import codecs

# ******************* Config Section ***********************


# you have to create a file called config.py in the same directory with the following information:
##  consumer_key="your key"
##  consumer_secret="your key"

## # After the step above, you will be redirected to your app's page.
## # Create an access token under the the "Your access token" section
##  access_token="your key"
##  access_token_secret="your key"

# import twitter keys and tokens
from config import consumer_key
from config import consumer_secret
from config import access_token
from config import access_token_secret


# ******************* Config Section ***********************


# create instance of elasticsearch
es = Elasticsearch([{'host' : 'localhost', 'port': 9200}])


class TweetStreamListener(StreamListener):

    def __init__(self, listTerms):
        self.listTerms = list_terms

    # on success
    def on_data(self, data):
        # decode json
        dict_data = json.loads(data)
        print dict_data["text"]

        # pass tweet into TextBlob
        tweet = TextBlob(dict_data["text"])

        # output sentiment polarity
        print tweet.sentiment.polarity

        # determine if sentiment is positive, negative, or neutral
        if tweet.sentiment.polarity < 0:
            sentiment = "negative"
        elif tweet.sentiment.polarity == 0:
            sentiment = "neutral"
        else:
            sentiment = "positive"

        # output sentiment
        print sentiment


        # add text and sentiment info to elasticsearch
        es.index(index="twitter2",
                 doc_type="tweets",
                 body={"author": dict_data["user"]["screen_name"],
                       "date": dict_data["created_at"],
                       "message": dict_data["text"],
                       "polarity": tweet.sentiment.polarity,
                       "subjectivity": tweet.sentiment.subjectivity,
                       "sentiment": sentiment,
                       "geoenabled": dict_data["user"]["geo_enabled"],
                       "coordinates": dict_data["coordinates"],
                       "geo" : dict_data["geo"],
                       "followerscount" : dict_data["user"]["followers_count"],
                       "retweeted" : dict_data["retweeted"]})

        return True

    # on failure
    def on_error(self, status):
        print status


if __name__ == '__main__':


    # create Terms to track
    #list_terms =["$AAPL", "$MSFT", "$DAX", "$DTEGY", "$BASFY", "$GOOG"]

    list_terms = ["hhngvss"]

    # create instance of the tweepy tweet stream listener
    listener = TweetStreamListener(list_terms)

    # set twitter keys/tokens
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)



    def start_stream(list_terms, auth, listener):
        while True:
            try:
                 # create instance of the tweepy stream
                stream = Stream(auth, listener,  timeout=300)

                # search twitter for  keyword list
                stream.filter(track=list_terms)
            except Exception as e:
                print  time.strftime("%c")
                print e
                continue

    start_stream(list_terms,auth,listener)