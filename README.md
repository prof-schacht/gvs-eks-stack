Kibana4 Vagrant Machine - Projekt GvS
======================================

Das ist eine virtuelle Vagrant Maschine, die folgende Technologien fertig installiert hat:

* Kibana 4
* ElasticSearch 
* stream2es
* Einzelne Python Skripte um Tweets in die Elasticsearch laden zu können.


Installation
------------

Um die Maschine starten zu können muss auf dem Rechner folgende Software installiert werden:

* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com)
* [Python 2.7](https://www.python.org/ftp/python/2.7.10/python-2.7.10.msi)
*  Optional [PyCharm](http://download.jetbrains.com/python/pycharm-community-4.5.4.exe)

Wenn man die Python Scripte nicht in der virtuellen Umgebung auswerten möchte, dann sollte 
an sich ergänzend die Programmierumgebung PyCharm für Python herunterladen.

* [PyCharm](https://www.jetbrains.com/pycharm/download/)

Verwendung
-----

Zunächst sollte das Repository von Bitbucket auf Ihren Rechner geladen werden:

* Legen Sie einen Ordner an
* Laden Sie das Repository von [Link](https://bitbucket.org/prof-schacht/gvs-eks-stack/downloads) herunter und entpacken Sie die Daten in den von Ihnen angelegten Ordner

Nachdem Sie Vagrant und Python installiert haben sollten Sie zunächst die notwendigen 
Python Bibliotheken installiert werden:

* Aufruf von PyCharm -> Untere Bildleiste auf PythonConsole klicken. (Gegebenenfalls brauchen Sie hier Administratorrechte)
* Installierung von Tweepy: pip install tweepy
* Installierung von Textblob: pip install -U textblob
* Installierung von elasticsearch: pip install elasticsearch


Im nächsten Schritt öffnen Sie eine Kommandozeile und wechseln in den von Ihnen angelegten Ordner (Ausführen >> CMD)

* Wechseln Sie in Ihren Ordner  (cd ...)
* Starten Sie die Umgebung mit "vagrant up"
* Mit "vagrant halt" und "vagrant up" können Sie die Umgebung pausieren und wieder starten.

Sie können nun die Analyseplattform mittels den folgenden Links im Browser erreichen:

* http://localhost:9200/_kopf (Elasticsearch Analyse-Frontend zur direkten Abfrage der Daten)
* http://localhost:5601/ (Kibana 4 - Analyse und Visualisierung Tool)

Um die Umgebung zu zerstören und gegebenenfalls von vorne anzufangen, können Sie den Befehl:

* "vagrant destroy" in dem Ordner vornehmen.


Möchten Sie nun einzelne Tweets in die Umgebung laden müssen Sie die Python-Skripte, die
in dem Ordner Skripte liegen unter PyCharm ausführen:

* streamTweetsByKeyWord (Sie können Keywords definieren die dann in Echtzeit in die Analyseumgebung geladen werden.)
* searchTweetsByWord (Sie können Keywords angeben, das Programm sucht dann bis zu 1800 Tweets der letzten Tage zu diesem Keyword heraus)
* TweetsByUser (Sie können einen TwitterUser angeben. Das Programm lädt alle Tweets dieses Users in die Analyseumgebung)

Wichtig ist, dass die Skripte nur dann funktionieren, wenn die Datei config.py die notwendigen Authentifizierungsschlüssel 
enthält.

Gute Tutorials für die Umgebung sind: 

* [Kibana](https://www.timroes.de/2015/02/07/kibana-4-tutorial-part-1-introduction/)
* [ElasticSearch Suche](https://www.elastic.co/guide/en/elasticsearch/reference/1.7/_introducing_the_query_language.html)
